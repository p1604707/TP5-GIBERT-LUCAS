window.addEventListener("load", function() {
	// Récupération du lecteur
	var lecteur = document.getElementById("lecteur");

	// Listener
	lecteur.addEventListener("timeupdate", updateProgressBar);
	document.getElementById("loading").addEventListener("click", load);

	document.getElementById("play").addEventListener("click", play);
	document.getElementById("pause").addEventListener("click", pause);

	function play() {
		// Si le lecteur n'a pas de podcast en cours
        if(lecteur.getAttribute("src") === ""){
        	// Chargement d'un podcast
        	loading();
        }
        else{
            lecteur.play();
            // Musique lancé
        }
	}

	function pause() {
        if(lecteur.played){
            lecteur.pause();
            // Musique en pause
        }
	}

	// Enlève le podcast courant et la photo
	// Relance par la même occasion
	function stop() {
		lecteur.pause();
		let progressBar = document.getElementById("progress-bar");
		progressBar.style.width = "0%";
        progressBar.textContent = "";
		lecteur.setAttribute("src", "");
		lecteur.setAttribute("poster", "");
		play();
	}

	// Load le mp3 et l'image
	function loading() {
        var listeLecture = document.getElementById('liste-lecture');
        var childrens = listeLecture.children;
        // Si il y a un item dans la liste
        if (childrens.length >= 1){
            let item = childrens[0];
            // Ajout IMG et AUDIO
            lecteur.setAttribute("poster", item.getElementsByTagName("p")[1].innerHTML);
            lecteur.setAttribute('src', item.getElementsByTagName("p")[0].innerHTML);
            lecteur.load();

            // Ajout listener pour supprimer la musique
            listeLecture.getElementsByTagName("button")[0].addEventListener("click", stop);
            play();
        }
        else {
        	console.log("aucune musique dans la liste");
        }
	}

	// Met à jour la progressBar
	function updateProgressBar() {
        let duration = lecteur.duration;
        let currentTime = lecteur.currentTime;
        let fraction = currentTime / duration;
        let percent = precisionRound(fraction * 100, 2);
        if(isNaN(percent)) {
        	percent = 0;
        }

        // Ajout
        let progressBar = document.getElementById('progress-bar')
        progressBar.style.width = percent + '%';
        progressBar.textContent = percent + '%';

        //Quand on arrive à la fin de l'audio :
        if(duration - currentTime <= 0){
	        stop();
        }

        // Fonction anonyme pour des jolis nombres ;)
        function precisionRound(number, precision) {
		  var factor = Math.pow(10, precision);
		  return Math.round(number * factor) / factor;
		}
    }

    /************************* Liste des choix ***********************/

	// Fonction pour l'appel AJAX
	function load() {
		var url = "https://crossorigin.me/" + document.getElementById('input-flux').value;
		var req = new XMLHttpRequest();
		req.open("GET", url, true);

		req.responseType = "document";
		req.overrideMimeType("text/xml");

		req.onload = function() {
		    if (req.status === 200) {
		    	var data = req.responseXML;
		      	loadXML(data);
		    } else {
		      console.log("Erreur " + req.status);
		    }
		};
		req.onerror = function() {
		    console.log("Échec de chargement "+url);
		};
		req.send(null);
	}

	// Fonction de parsage XML et ajout sur la liste de choix
	function loadXML(data) {
	    // Delete la liste de choix actuel
	    var listeLecture = document.getElementById("liste-choix");
		listeLecture.innerHTML = "";

	    // Récupération de la liste
	    var tabItems = data.getElementsByTagName('item');
	    var i;
	    for(i = 0; i < tabItems.length; i++){
	    	// A Conteneur
	        let conteneur = document.createElement("a");
	        conteneur.className = "list-group-item list-group-item-action d-flex justify-content-between align-items-center";

	        // Span Titre
	        let title = document.createElement("span");
	        title.id = "title";
	        title.innerHTML = tabItems[i].getElementsByTagName('title')[0].innerHTML;
	        conteneur.appendChild(title);

	        // P Hidden MP3
	        let mp3 = document.createElement("p");
	        mp3.setAttribute("hidden", "hidden");
	        mp3.id = "mp3";
	        mp3.innerHTML = tabItems[i].getElementsByTagName('guid')[0].innerHTML;
	        conteneur.appendChild(mp3);

	        // P Hidden img
	        let img = document.createElement("p");
	        img.setAttribute("hidden", "hidden");
	        img.id = "img";
	        img.innerHTML = tabItems[i].getElementsByTagName('media:thumbnail')[0].getAttribute("url");
	        conteneur.appendChild(img);

	        // Bouton AJOUTER
	        let ajouter = document.createElement("button");
	        ajouter.className = "btn btn-success text-bold-2";
	        ajouter.innerHTML = "Ajouter";
	        ajouter.id = "ajouter";
	        ajouter.addEventListener("click", function() { addLecture(conteneur) });
	        conteneur.appendChild(ajouter);

	        listeLecture.appendChild(conteneur);
	    }
	}

	/************************* Liste de Lecture ***********************/

	function addLecture(conteneur) {
		// Récupération de l'élement
		let conteneurLecture = conteneur.cloneNode(true);
		
		// Supprime le bouton ajouter
		let ajouter = conteneurLecture.getElementsByTagName("button")[0];
		conteneurLecture.removeChild(ajouter);

		// Ajoute le bouton supprimer
	    let supprimer = document.createElement("button");
	    supprimer.className = "btn btn-danger text-bold-2";
	    supprimer.innerHTML = "Supprimer";
	    supprimer.id = "supprimer";
	    supprimer.addEventListener("click", function() { removeLecture(conteneurLecture) });
	    conteneurLecture.appendChild(supprimer);
		
		// Ajout à la liste de lecture
		let listeLecture = document.getElementById("liste-lecture");
		listeLecture.appendChild(conteneurLecture);

	    let childrens = listeLecture.children;
	    // Si il y a un item dans la liste
	    if (childrens.length == 1){
	    	play();
	    }
	}

	function removeLecture(conteneur) {
		let listeLecture = document.getElementById("liste-lecture");
		listeLecture.removeChild(conteneur);
	}

});